#!usr/bin/python

import socket
import os
import mimetypes
import string

# Defaults 
# host : localhost
# port : 6060 

# render could be static or dir
RENDER = 'dir'

def write_htmldoc(connfp):

	fp = open('doc.html', 'r')
	connfp.write('HTTP/1.0 200 OK\n\n')
	for line in fp:
		if not line:
			break
		connfp.write(line)
	fp.close()

def write_dircontents(connfp):

	connfp.write('HTTP/1.0 200 OK\n\n')
	connfp.write('<html> <body>')
	connfp.write('<head><title>Web Server</title></head>')
	connfp.write('<h1>Directory Listings</h1>')
	contents = os.listdir(os.curdir)
	for content in contents:
		connfp.write('<li>%s</li>' % content)
	connfp.write('</body></html>')


def setup_webserver(host='', port=6060):

	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	except socket.error as inst:
		print "Couldn't connect to host:port" % (host, port)
		raise inst

	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	# bind and listen for connections
	try:
		sock.bind((host, port))
		sock.listen(1)
	except socket.error as inst:
                sock.close()
                raise inst	

	# accept the connections
	while 1:
		(conn, addr) = sock.accept()
		conn.setblocking(0)		

		connfp = conn.makefile('rw', 0)
		if RENDER == 'static':
			write_htmldoc(connfp)
		elif RENDER == 'dir':
			write_dircontents(connfp)
		connfp.close()

		conn.shutdown(socket.SHUT_RDWR)
		conn.close()

setup_webserver()


