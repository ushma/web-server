#!usr/bin/python

import socket
import os
import mimetypes
import string

# Defaults 
# host : localhost
# port : 6060

def setup_server(host='', port=6060):

	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	except socket.error as inst:
		print "Couldn't connect to host:port" % (host, port)
		raise inst

	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	# bind and listen for connections
	try:
		sock.bind((host, port))
		sock.listen(1)
	except socket.error as inst:
		sock.close()
		raise inst
	
	# accept the connections
	while 1:
		(conn, addr) = sock.accept()
		conn.setblocking(0)	

		#read the data
		while 1:
			try:
				data = conn.recv(1024)
			except socket.error:
				break
			print data
			if not data:
				break

		conn.shutdown(socket.SHUT_RDWR)
		conn.close()

setup_server()


