#!usr/bin/python

import socket
import os
import mimetypes
import string

# Defaults 
# host : localhost
# port : 6060 

# render could be static or dir
RENDER = 'dir'
root = os.curdir

def write_htmldoc(connfp):

	fp = open('doc.html', 'r')
	connfp.write('HTTP/1.0 200 OK\n\n')
	for line in fp:
		if not line:
			break
		connfp.write(line)
	fp.close()

def write_dircontents(connfp, curdir):

	connfp.write('HTTP/1.0 200 OK\n\n')
	connfp.write('<html> <body>')
	connfp.write('<head><title>Web Server</title></head>')
	connfp.write('<h1>Directory Listings</h1>')	
 	
	contents = os.listdir(curdir)
	print 'Listing %s dir contents' % curdir

	for content in contents:
		# if dir create a link
		abspath = os.path.join(curdir, content)
		if os.path.isdir(abspath):
			connfp.write("<li><a href=%s/>%s</a></li>" % 
							(content, content))
		else:	
			connfp.write('<li><a href=%s>%s</a></li>' % 
							(content, content))
	connfp.write('</body></html>')


def check_content(path):

	abspath = os.path.join(root, path[1:])
	if os.path.isdir(abspath):
		return (abspath, 'dir', None, None)
	else:
		(_type, encoding) = mimetypes.guess_type(abspath)
		size = os.stat(abspath).st_size
		return (abspath, _type, encoding, size)


def copy_obj(src, dst):

	fp = open(src, 'rb')
	while True:
		data = fp.read(1024)
		if not data:
			break
		dst.write(data)
	fp.close()


def setup_webserver(host='', port=6060):

	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	except socket.error as inst:
		print "Couldn't connect to host:port" % (host, port)
		raise inst

	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	# bind and listen for connections
	try:	
		sock.bind((host, port))
		sock.listen(1)
	except socket.error as inst:
                sock.close()
                raise inst	

	# accept the connections
	while 1:
		(conn, addr) = sock.accept()
		conn.setblocking(0)

		connfp = conn.makefile('rw', 0)

		try:	
			data = connfp.readline().strip()
		except socket.error:
			pass

		path = string.split(data, ' ', 3)[1]
		(abspath, _type, encoding, size) = check_content(path)
 
		if _type == 'dir':
			write_dircontents(connfp, abspath)
		else:
			connfp.write('HTTP/1.0 200 OK\n')
			if encoding:
				connfp.write('Content-Encoding: %s\n' % encoding)
			connfp.write('Content-Type: %s\n' % _type)
			connfp.write('Content-Length: %s\n\n' % size)
			copy_obj(abspath, connfp)
		
		connfp.close()

		conn.shutdown(socket.SHUT_RDWR)
		conn.close()

setup_webserver()


