#!usr/bin/python

import socket
import os
import mimetypes
import string

# Defaults 
# host : localhost
# port : 6060 

# render could be static or dir
RENDER = 'dir'
root = os.curdir

def write_htmldoc(connfp):

	fp = open('doc.html', 'r')
	connfp.write('HTTP/1.0 200 OK\n\n')
	for line in fp:
		if not line:
			break
		connfp.write(line)
	fp.close()

def write_dircontents(connfp, path):

	connfp.write('HTTP/1.0 200 OK\n\n')
	connfp.write('<html> <body>')
	connfp.write('<head><title>Web Server</title></head>')
	connfp.write('<h1>Directory Listings</h1>')	
 	
	curdir = os.path.join(root, path[1:])	
	contents = os.listdir(curdir)
	print 'Listing %s dir contents' % curdir

	for content in contents:
		# if dir create a link
		abspath = os.path.join(curdir, content)
		if os.path.isdir(abspath):
			connfp.write("<li><a href=%s/>%s</a></li>" % 
							(content, content))
		else:	
			connfp.write('<li>%s</li>' % content)
	connfp.write('</body></html>')


def setup_webserver(host='', port=6060):

	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	except socket.error as inst:
		print "Couldn't connect to host:port" % (host, port)
		raise inst

	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	# bind and listen for connections
	try:	
		sock.bind((host, port))
		sock.listen(1)
	except socket.error as inst:
                sock.close()
                raise inst	

	# accept the connections
	while 1:
		(conn, addr) = sock.accept()
		conn.setblocking(0)

		connfp = conn.makefile('rw', 0)

		try:	
			data = connfp.readline().strip()
		except socket.error:
			pass
	
		if RENDER == 'static':
			write_htmldoc(connfp)
		elif RENDER == 'dir':
			path = string.split(data, ' ', 3)[1]
			write_dircontents(connfp, path)
		
		connfp.close()

		conn.shutdown(socket.SHUT_RDWR)
		conn.close()

setup_webserver()


